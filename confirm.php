<?php
	session_start();
?>
<!DOCTYPE html>
<html>
<head>
<title>Day05</title>

<style>
input {
  padding: 15px 15px;
  margin: 8px 0;
  box-sizing: border-box;
}
div.b {
  background-color: white;
  padding: 0px;
  border: 2px solid indigo;
  margin: 15px;
}
div.c{
	text-align:center;
}
span.d {
  display:inline-block;
  background-color:limegreen;
  color:white;
  height: 20px;
  width:100px;
  padding: 15px;
  border: 1px solid indigo;
  margin: 15px;
}
.button {
  background-color: limegreen;
  border: 1px solid indigo;
  color: white;
  margin:15px;
  width:100px;
  padding: 15px;
  text-align: center;
  display: inline-block;
  font-size: 16px;
}
</style>
</head>

<body>
	<div class = "b">
		<div class = "container">
			<span class = "d"> Họ và tên </span>
			<?php echo $_SESSION ["name"]; ?>
		</div>
		<div class = "container">
			<span class = "d"> Giới tính </span>
			<?php echo $_SESSION ["gender"]; ?>
		</div>
		<div class = "container">
			<span class = "d"> Phân khoa </span>
			<?php
				$arr = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
				$key = $_SESSION ["khoa"];
				echo $arr[$key];
			?>
			</select>
		</div>
		<div class = "container">
			<span class = "d"> Ngày sinh </span>
			<?php
			$date = date_create($_SESSION ["birthday"]);
			echo date_format($date, "d/m/Y");
			?>
		</div>
		<div class = "container">
			<span class = "d"> Địa chỉ </span>
			<?php echo $_SESSION ["address"]; ?>
		</div>
		<div class = "container">
			<span class = "d"> Hình ảnh </span>
			<img alt="picture" src="<?php echo $_SESSION["myfile"]; ?>"/>
		</div>
		<div class = "c">
			<button class = "button">Xác nhận</button>
		</div>
	</div>
</body>
</html>